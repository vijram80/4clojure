(ns sandbox.4clojure-test
  (:require [clojure.test :refer :all]
            [sandbox.4clojure :refer :all]))

(deftest fib-test
  (testing "4clojure problem 26"
           (is (= [1 1 2 3 5 8 13 21] (fib 8)))))

(deftest fib2-test
  (testing "4clojure problem 26 solutions are same"
           (is (= (fib 10) (fib2 10)))))