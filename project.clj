(defproject sandbox "0.1.0-SNAPSHOT"
  :description "Sample project with Java and Clojure code"
  :url "http://10.189.6.90:8000/john.krupka/sandbox"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :min-lein-version "2.0.0"
  :source-paths ["src/clojure"]
  :java-source-paths ["src/java"]
  :javac-options     ["-target" "1.6" "-source" "1.6"]
  :main ^:skip-aot sandbox.mainclj
  :dependencies [[org.clojure/clojure "1.6.0"]]
  :profiles {:uberjar {:aot :all}})
