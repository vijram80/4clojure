(ns sandbox.test)

(defn fn1 [xs] 
  (println 
    xs
    " test")
  (apply + xs))


(defn inc-more [nums]
  (if (first nums)
    (cons (inc (first nums))
          (inc-more (rest nums)))
    (list)))

(def quintuple (partial * 5))

(def asym-hobbit-body-parts [{:name "head" :size 3}
                             {:name "left-eye" :size 1}
                             {:name "left-ear" :size 1}
                             {:name "mouth" :size 1}
                             {:name "nose" :size 1}
                             {:name "neck" :size 2}
                             {:name "left-shoulder" :size 3}
                             {:name "left-upper-arm" :size 3}
                             {:name "chest" :size 10}
                             {:name "back" :size 10}
                             {:name "left-forearm" :size 3}
                             {:name "abdomen" :size 6}
                             {:name "left-kidney" :size 1}
                             {:name "left-hand" :size 2}
                             {:name "left-knee" :size 2}
                             {:name "left-thigh" :size 4}
                             {:name "left-lower-leg" :size 3}
                             {:name "left-achilles" :size 1}
                             {:name "left-foot" :size 2}])

(defn needs-matching-part?
  [part]
  (re-find #"^left-" (:name part)))

(defn make-matching-part
  [part]
  {:name (clojure.string/replace (:name part) #"^left-" "right-")
   :size (:size part)})

(defn symmetrize-body-parts
  [asym-body-parts]
  (loop [remaining-asym-parts asym-body-parts
         final-body-parts []]
    (if (empty? remaining-asym-parts)
      final-body-parts
      (let [[part & remaining] remaining-asym-parts
            final-body-parts (conj final-body-parts part)]
        (if (needs-matching-part? part)
          (recur remaining (conj final-body-parts (make-matching-part part)))
          (recur remaining final-body-parts))))))


(def vowel? (set "aeiou"))

(def square (partial map #(* % %)))

(take 10 
      (map #(* % %) 
           (filter odd? 
                   (range))))

(->> (range)
  (filter odd?)
  (map #(* % %))
  (take 10))

(defn pig-latin [word]
  (let [first-letter (first word)]
    (if (vowel? first-letter)
      (str word "ay")
      (str (subs word 1) first-letter "ay"))))


((fn [x] (first (into [](#(reverse %) x))) ) [1 2 3 4 5])

;;(defn nth [x y] (first (drop x y)))

(defn fn1 [] (+ 2 3))

(defn mycount [x y] (when (first x)) (let [y 0] (mycount (rest x) y)))


(defn newcount [x] 
  (loop [remaining x total 0]
    (if (empty? remaining)
      total
      (let [[_ & remaining-list] remaining
            total (+ 1 total)]
        (recur remaining-list total)))))



(reverse (reduce (fn flat [t v] (if (coll? v) (reduce (fn [t1 v1] (conj t1 v1)) t v) (conj t v))) '() '((1 2) 3 [4 [5 6]])))

(defn f [lst]
  (loop [t [] v lst]
    (let [el (first v)]
      (cond
        (empty? v) t
        (coll? el) (recur t (concat el (rest v)))
        :else (recur (conj t el) (rest v))))))


(reduce (fn [t v] (if (re-seq #"[A-Z]+" v) (apply t v) t)) "" "HeLLo WorLd")

(apply str (reduce (fn [t v] (if (= (last t) v) t (conj t v))) [] [1 1 2 3 3 2 2 3]))

(reduce (fn [t v] 
          (if (= (first t) v) 
            (concat '() t (conj '() v)) 
            (if (empty? t) 
              (conj t v) 
              (concat '() t (conj '() v))))) '() [1 1 2 1 1 1 3 3])


(defn dupl [lst]
  (loop [t '() temp '() v lst]
    (let [el (first v)]
      (cond
        (empty? v) t
        (empty? t) (conj t v)
        (= (last t) el) (recur t (concat temp (conj '() el)) (rest v))
        :else (recur (conj t el) '() (rest v))))))


(->> [1 2 3 4 5]
  (partition-all 1)
  ())

;(reduce (fn flat [t v] (if (coll? v) (reduce into v) (conj t v))) '() '((1 2) 3 [4 [5 6]]))

reduce (fn [t _] (inc t)) 0

(apply str (reduce (fn [t v] (cons v t)) '() "abcd"))


(fn rev [x] (if (> (count x) 0) (cons (last x) (rev (drop-last x)))))

(reduce (fn [t v] (+ t v)) 0 [1 2 3]) 


(reduce (fn [t v] (if (odd? v) (conj t v) t)) '() [1 2 3 4 5])


(defn fib [n] 
  (loop [x 1 y 1 z 2 k '(1 1)] 
    (if (== n z)
      k
      (recur y (+ x y) (inc z) (conj (vec k) (+ x y))))))

(fn pal [x]
  (if (string? x)
  (= (apply str (reverse x)) x)
  (= x (reverse x))))
  

(reverse (drop 2 "hello"))

(-> "hello"
(#(drop 2 %))
(reverse))

((comp reverse (partial drop 2 )) "hello")

(reductions (fn [t v] (partition-all #(= (last t) v) t) )  '() [1 1 1 2 2 3 3]) 
  
(reduce (fn [t v] (partition-by  #(= (last t) v) )) '()  [1 1 1 2 2 3 3])

(reduce (fn [t v] (partition-by  (partial identical?  (last t)) v )) '()  [1 1 1 2 2 3 3])


(partition-by (partial identical? 1) [1 1 2 1 1 1 3 3])  

(#(reverse (reduce (fn [t v] (conj t v v)) '() %)) [[1 2] [3 4]])



;(mapcat (fn [o n] (repeat n (list o o))) [1 2 3] 2)

(#(mapcat (repeat %2 %1))  [1 2 3] 2)

(mapcat (fn [o n] (repeat n o)) [1 2 3] 2)
((fn [o n] (repeat n o)) [1 2 3] 2)
      
(reduce #(repeat 2 (conj %1 %2 %2)) '() [1 2 3] )  

(defn rep [x n]
  (loop [n n x x]
    (if (<= n 1)
    x
    (recur (- n 2) (mapcat (fn [o] (list o o)) x )))))

(defn incy [x n]
  (if (= x (- n 1))
    x
    (flatten (conj '() (incy (inc x) n) x ))))

(#(reduce (fn [t v] (if (> v t) v t)) %&) 45 67 11)


(defn inter [x y]
  (loop [tot '() x x y y]
    (if (or (empty? x)  (empty? y))
    (reverse tot)
    (recur (conj tot (first x) (first y) ) (rest x) (rest y)))))

;; or
(mapcat list)


(defn interp [x y]
  (loop [tot '() x x y y]
    (if (empty? y)
      (drop-last (reverse tot))
      (recur (conj tot (first y) x) x (rest y))))) 

;;or
(#(rest (interleave (repeat %1) %2)) :z [:a :b :c :d])


(defn dropy [x y]
  (loop [tot '() x x y y]
    (if (empty? x)
      (flatten tot)
      (recur (concat (conj (take (- y 1) x) tot)) (drop y x) y))))

;;or
(fn [coll n]
  (mapcat #(take (dec n) %) (partition-all n coll)))

 (#(= (if (true? (contains? %2 %1)) (get %2 %1) false ) nil)  :a {:c nil :b 2})

(#(apply hash-map (concat (interpose %1 %2) [%1])) 0 [:a :b :c])
 ;;or
#(into {} (for [x %2] [x %1]))


(->> [1 2 3 4 5 6]
     (take 3)
     (rest (partial drop 3)))

;;problem 83


(#(> (count (distinct [%&])) 1) true false) 

;; create map
(#(apply hash-map (interleave %1 %2))[:foo :bar] ["foo" "bar" "baz"])

;;greatest common divisor
(defn gcd[x y]
  (loop [d (min x y)]
    (if (and (= (rem x d) 0) (= (rem y d) 0)) 
      d
      (recur (dec d)))))



(fn f1 [fn1 v] (cons v (lazy-seq (f1 fn1 (fn1 v)))))


(fn ex [n] #(reduce * (repeat n %)))


(defn mop [n1 n2]
  (loop [val (* n1 n2) tot '()]
    (if (= val 0)
      tot
      (recur (quot val 10) (conj tot (rem val 10))))))
    
(#(map read-string (re-seq #"\d+" (str (list (partition 1 (str (* %1 %2))))))) 99 9)

(#(into #{} (for [x %1 y %2] [x y])) #{"ace" "king" "queen"} #{"♠" "♥" "♦" "♣"})



  
  
(defn mygroup [func x]
(let [kvals (interleave (map func x) x)]
  (loop [result {} kvals kvals]
    (if (empty? kvals)
    result
    (recur (if (contains? result (first kvals))
             (assoc result (first kvals) (conj (get result (first kvals)) (second kvals)))
             (assoc result (first kvals) (conj [] (second kvals)))) (rest (rest kvals))))))) 
            
  ;; or
  (fn [f coll]
  (reduce #(merge-with into %1 {(f %2) [%2]}) {} coll))
  
  (#(java.lang.Integer/parseInt % 2) "111")
  
  (#(clojure.set/difference (clojure.set/union %1 %2) (clojure.set/intersection %1 %2)) #{:a :b :c} #{})
  
  
  
  
  
(defn myfun [x y]
(loop [tot [] x x y y]
(if (empty? x)
(apply + tot)
(recur (conj tot (* (first x) (first y))) (rest x) (rest y)))))

;;or
(reduce + (map * [1 2 3]  [4 5 6]))
  
(and (= (class Class) Class) Class)


;(= 42 (__ 38 + 48 - 2 / 2))
;+, -, *, and /

(defn infix [& x]
  (loop [tot ((second x) (first x) (nth x 2)) x (drop 3 x)]
        (if (empty? x)
          tot
          (recur ((first x) tot (second x))  (drop 2 x)))))


(#(map (fn [x y] [x y]) % (iterate inc 0)) [:a :b :c])
;or
map-indexed #(vector %2 %1)


;;pascal        


(defn pas [x]
  (loop [tot [1] count 1]
    (if (= x count)
      tot
      (recur (flatten (conj [] (first tot) (map (fn [x y] (+ x y)) tot (rest tot)) 1)) (inc count)))))


(defn pascal [row]
  (if (= row 1)
    [1]
    (for [x (range row)]
      (let [prev-row (pascal (dec row))]
        (+ (nth prev-row (dec x) 0) (nth prev-row x 0))))))



;;map
  
(defn f2 [fn1 v]
  (if (not(empty? v))
  (cons (fn1 (first v)) (lazy-seq (f2 fn1 (rest v))))))
  
  ;;or
(fn [f coll] (rest (reductions #(f %2) nil coll)))

;;longest subseq
(defn myseq [x]
  (loop [tot [(first x) ] result [] x (drop 1 x)]
    (prn x tot  result) 
    (if (empty? x)
    (reduce (fn [t v] (if (and (> (count v) (count t)) (> (count v) 1)) v t )) [] result)
    (if (= (range (first tot) (inc (last tot))) tot)
      (if (= (conj tot (first x)) (range (first tot) (inc (first x))))
         (recur (conj tot (first x)) (conj result (conj tot (first x)))  (drop 1 x) )
         (recur (conj [] (first x) ) (conj result  tot ) (drop 1 x)))
      (recur (conj [] (first x)  ) result (drop 1 x))
      ))))
;;or
(fn [s]
  (or (first (filter #(apply < %) (mapcat #(partition % 1 s) (range (count s) 1 -1))))
      []))
;;or
(sort-by count (reductions (fn [t v] (if (or (empty? t) (= v (inc (last t)))) (conj t v) [v])) [] [1 0 1 0 2 0 3 0 4 5]))

   
(#(reduce (fn [t v] (let [sqvale (reduce (fn [t v] (+ t (* v v))) 0 (map read-string (re-seq #"\d+" (str (list (partition 1 (str v)))))))]
         (if (> sqvale v) (inc t) t ))) 0 %) (range 10))
          
;;to get numeric values
(map #(Character/getNumericValue %) (str 010))
  
;;binary-tree
(fn tree? [x] (or (nil? x) (and (sequential? x) (= 3 (count x)) (tree? (second x)) (tree? (last x)))))
;;or
(#(odd? (count (remove false? (flatten %)))) [1 [2 [3 [4 false nil] nil] nil] nil])

;;playing cards
(defn pc [[s r]]
  (let [rank (zipmap "23456789TJQKA" (range 0 13)) suit (zipmap "DHCS" [:diamond :heart :club  :spade])]
    (assoc {} :suit (suit s) :rank (rank r))))
       
;;destructuring
(= 3
  (let [[x y] [+ (range 3)]] (apply x y))
  (let [[[x y] b] [[+ 1] 2]] (x y b))
  (let [[x y] [inc 2]] (x y)))



;;nth-pascal (= (second (__ [2 3 2])) [2 5 5 2])
(defn npas [x]
  (iterate #(flatten (conj [] (first %) (map (fn [a b] (+' a b)) % (rest %)) (last %))) x))



;;pairwise disjoint sets    
(fn [x]
  (loop [result #{} s1 (first x) s2 (drop 1 x)]
    (if (empty? s2)
      (= (count result) 1) 
      (recur (conj result (map #(clojure.set/intersection s1 %) (disj x s1))) (first s2) (drop 1 s2)))))


;;tree into tables
(for [x (keys (into {} '{a {p 1, q 2} b {m 3, n 4}})) y (vals (into {} '{a {p 1, q 2} b {m 3, n 4}})) :when ] [x y])



(for [y (keys (into {} '{m {1 [a b c] 3 nil}})) z (vals (into {} '{m {1 [a b c] 3 nil}}))] [[y (ffirst z)] (second (first z))])       


;;flip arguments
(defn flip [f]
  (fn [& args] (apply f (reverse args))))

;;rotate sequence
(defn rot1 [x y]
  (take (count y) (drop (if (neg? x) (inc (max x (- x))) x) (flatten (repeat (count y) y)))))


;;reverse interleave
(defn rint [x y]
  (loop [result [] x x counter 1 ]
    (if (> counter y)
      result
      (recur (conj result (take-nth y x)) (drop 1 x) (inc counter))))) 


;;comparisons
(defn dum [o x y]
  (if (o x y)
    :lt
    (if (o y x)
      :gt
      :eq)))



(def kvls (into {} (map hash-map [1 :a 2 :b 3 :c] (map #(type %) [1 :a 2 :b 3 :c]))))
(reduce-kv #(assoc %1 %3 %2) {} (into {} (map hash-map [1 :a 2 :b 3 :c] (map #(type %) [1 :a 2 :b 3 :c]))))
;;group-by same item in collection
(#(vals (group-by (partial type ) %)) [1 :a 2 :b 3 :c])

;;count occurences
(#(reduce (fn [t v] (assoc t (first v) (count (second v)))) {} (group-by identity %)) [1 1 2 3 2 1 1]) 

;;Find Distinct Items
((fn [x] (reduce (fn [t v] (if (some #(= v %) t) t (conj t v))) [] x)) [:a :a :b :b :c :c])


;;compose functions
(defn mycomp [& fs]
  (fn [& y] (reduce (fn [t v] (v t)) (apply (last fs) y) (rest (reverse fs))))) 


;;partition
(defn part1 [ n c ]
  (loop [c c result []]
    (if (< (count c) n)
      result
      (recur (drop n c) (conj result (take n c))))))
                        
                        
;;juxtaposition                       
(defn mycomp1 [& fs]
  (fn [& y] (reduce (fn [t v] (conj t (apply v y))) [] fs)))                       
;;or
(fn jxt [& fs] (fn [& a] (map #(apply % a) fs)))


;;sort words in a collection
#(sort-by clojure.string/upper-case (re-seq #"[A-Za-z]+" %))


;;prime-numbers
(fn [n]
  (loop [pn 2 result []]
    (if (= (count result) n)
      result
      (recur (inc pn) (if (nil? (some #(= 0 %) (map #(rem pn %) (range 2 pn)))) (conj result pn) result)))))

;;perfect-squares
(fn [x]
  (loop [result "" c (map read-string (re-seq #"\d+" x)) ]
    (if (empty? c)
      (apply str (drop 1 result))
      (recur (if (some #(= (first c) %) (map #(* % %) (range 2 (first c)) )) (str result "," (first c)) result) (rest c)))))

;;black box testing
(fn [x]
  (let [c (first (str x))]
    (if (= c \{)
      :map
    (if (=  c \c)
      :list
     (if (= c \[)
      :vector
      (if (= c \#)
      :set))))))

;;perfect-numbers
#(= (apply + (reduce (fn [t v] (if (= (rem % v) 0) (conj t v) t)) [] (range 1 %))) %)

;;reductions
(defn myr [f & y]
  (if (= (count y) 1)
    (lazy-seq (myr f (first y) (rest y)))
    (let [[c1 c2] y]
      (cons c1 (if (not (empty? c2))
                 (lazy-seq (myr f (f c1 (first c2)) (rest c2))))))))



(defn myred
  ([f c]
    (lazy-seq (myred f (first c) (rest c))))
  ([f c1 c2] (prn c1 c2) (cons c1 (if (not (empty? c2))
                                    (lazy-seq (myred f (f c1 (first c2)) (rest c2)))))))

;;merge-with
(= (__ * {:a 2, :b 3, :c 4} {:a 2} {:b 2} {:c 5})
   {:a 4, :b 6, :c 20})
(__ - {1 10, 2 20} {1 3, 2 10, 3 15})
   {1 7, 2 10, 3 15}
(= (__ concat {:a [3], :b [6]} {:a [4 5], :c [8 9]} {:b [7]})
   {:a [3 4 5], :b [6 7], :c [8 9]})

(fn [f m1 & m2]
  (let [m2 (mapcat concat m2)]
      (reduce (fn [t val] (let [[k v] val] (if (contains? t k) (update-in t [k] f v) (assoc t k v)))) m1 m2)))


;;camelcase
(defn camelcase [x]
  (if (re-find #"-" x)
    (let [arr (clojure.string/split x #"-")]
      (prn (first arr) (rest arr))
    (reduce (fn [t v] (prn v) (str t (clojure.string/capitalize v) )) (first arr) (rest arr)))
    x))

(defn camelcase1 [x]
  (let [arr (clojure.string/split x #"-")]
    (reduce (fn [t v] (prn v) (str t (clojure.string/capitalize v) )) (first arr) (rest arr))))

    
; anagram
(defn anagram [xs]
  (loop [x (first xs) y (rest xs) z #{}]
    (prn x)
    (if (empty? y)
      z
      (recur (first y) (rest y) z))))


;(= 827 (__ "DCCCXXVII"))
;(= 48 (__ "XLVIII"))
;(= 14 (__ "XIV"))
;(= 3999 (__ "MMMCMXCIX"))


;roman numbers
(defn roman [xs]
  (let [ n {\I 1 \V 5 \X 10 \L 50 \C 100 \D 500 \M 1000} ]
  (loop [f (first xs) s (rest xs) res 0 p nil]
    (if (nil? f) 
      res
      (do (let [t (get n f)] (recur (first s) (rest s) (if (nil? p) (+ res t) (if (> t p)  (- (+ res (- t p)) p)  (+ res t))) t)))))))
      
    
;(= "DCCCXXVII" (__ 827))
;(= "MMMCMXCIX" (__ 3999))
;(= "XLVIII" (__ 48))  



;xs (map #(java.lang.Character/getNumericValue %) (str xs))
;(let [xs (#(java.lang.Integer/parseInt % ) (str xs))]
;(let n {1000 "M" 900 "CM" 500 "D" 400 "CD" 100 "C" 90 "XC" 50 "L" 40 "XL" 10 "X" 9 "IX" 5 "V" 4 "IV" 1 "I"}
;reverse of roman
(defn rev-roman [xs]
  (loop [xs xs res ""]
    (if (<= xs 0)
      res
      (cond
        (>= xs 1000) (recur (- xs 1000) (str res "M"))
        (>= xs 900) (recur (- xs 900) (str res "CM"))
        (>= xs 500) (recur (- xs 500) (str res "D"))
        (>= xs 400) (recur (- xs 400) (str res "CD"))
        (>= xs 100) (recur (- xs 100) (str res "C"))
        (>= xs 90) (recur (- xs 90) (str res "XC"))
        (>= xs 50) (recur (- xs 50) (str res "L"))
        (>= xs 40) (recur (- xs 40) (str res "XL"))
        (>= xs 10) (recur (- xs 10) (str res "X"))
        (>= xs 9) (recur (- xs 9) (str res "IX"))
        (>= xs 5) (recur (- xs 5) (str res "V"))
        (>= xs 4) (recur (- xs 4) (str res "IV"))
        (>= xs 1) (recur (- xs 1) (str res "I"))))))



; #132 insert between two  items
;(defn ins2 [op kw xs]
;  (flatten ((fn ins1 [op kw prev xs]
;              (cons ((fn [[x y]] (prn x y (count xs))
;                       (if (and (= x _) (= y nil))
;                         x
;                         (if (nil? y) []  
;                           (if (op x y) 
;                             (if (= prev x) (conj [] kw y) (conj [] x kw y)) 
;                             (if (= prev x) (conj [] y) (conj [] x y)))))) (take 2 xs)) 
;                    (if (not-empty xs) 
;                      (lazy-seq (ins1 op kw (second xs) (drop 1 xs)))))) op kw -1 xs)))

;(= '(1 :less 6 :less 7 4 3) (__ < :less [1 6 7 4 3]))
;(= '(2) (__ > :more [2]))
;(= [0 1 :same 1 2 3 :same 5 8 13 :same 21]
   (take 12 (->> [0 1]
                 (iterate (fn [[a b]] [b (+ a b)]))
                 (map first) ; fibonacci numbers
                 (ins3 (fn [a b] ; both even or both odd
                         (= (mod a 2) (mod b 2)))
                     :same)))

(defn ins3 [op kw xs]
  (flatten
   (lazy-cat
     (map (fn [[x y]] (prn x y) (if (nil? y) [x] (if (op x y) [x kw] x))) (partition-all 2 1 xs)))))



;;semi flatten sequence
(defn flat1 [xs]
  (letfn [(flat [ys]
          (loop [ys (partition 1 ys) res []]
            (if (empty? ys)
              res
              (recur (rest ys) (conj res (flatten (first ys)))))))]
    (reduce (fn [t v] (prn t v) (concat t (if (coll? (first v)) (flat v) (conj [] v)))) [] xs)))


(defn flatter [xs]
  (reduce (fn [t v] (if (and (coll? v) (not (coll? (first v))))  (conj t v) t)) [] (tree-seq coll? identity xs)))


  

;; oscilrate
;;(= (take 3 (__ 3.14 int double)) [3.14 3 3.0])
;;(= (take 12 (__ 0 inc dec inc dec inc)) [0 1 0 1 0 1 2 1 2 1 2 3])
(defn oscc [& ys]
  (lazy-seq
    (loop [x (first ys) res [x] xs (rest ys)]
      (recur ((first xs) x) (conj res ((first xs) x)) (flatten (conj (vector (drop 1 xs)) (first xs))))))) 

;;working function
(defn osc [x & xs]
  (if (seq? x)
    (do (let [y (first x) zs (rest x)]
          (cons ((first zs) y) (lazy-seq (osc (cons ((first zs) y) (flatten (conj (vector (drop 1 zs)) (first zs)))))))))
    (cons x (cons ((first xs) x) (lazy-seq (osc (cons ((first xs) x) (flatten (conj (vector (drop 1 xs)) (first xs))))))))))


(defn osc1 [& ys]
  (let [zs (vector ys) x (first zs) xs (rest zs)]
    (prn x )
    (cons x (cons ((first xs) x) (lazy-seq (osc1 (cons ((first xs) x) (flatten (conj (vector (drop 1 xs)) (first xs))))))))))


;;john's haskell vs clojue task
;;let primes = sieve [2..] where sieve (x:xs) = x:(sieve [y | y <- xs, mod y x > 0])
(defn primes []
  (letfn [(sieve [xs]
            (cons (first xs) (lazy-seq (sieve (for [y (rest xs) :when (pos? (mod y (first xs)))] y)))))]
    (sieve (iterate inc 2))))

(defn primes []
  (letfn [(sieve [[x & xs]]
            (lazy-seq (cons x (sieve (for [y xs :when (pos? (mod y x))] y)))))]
    (sieve (iterate inc 2))))

;least common multiple
(defn lcd [& n]
  (letfn [(lcm [a b] (/ (* a b) (gcd a b)))
          (gcd [a b] (cond (zero? b) a :else (gcd b (mod a b))))]
  (reduce lcm n)))
;;or
(defn lcm2 [& xs]
  (loop [ys xs]
    (if (apply = ys)
      (first ys)
      (recur (map #(if (= %2 (apply min ys)) (+ %1 %2) %2) xs ys)))))


;;beauty is symmetry
;(= (__ '(:a (:b nil nil) (:b nil nil))) true)
(fn [[x y z]]
  (letfn [(swap [[a b c]] (conj [] a (if (vector? c) (swap c) c) (if (vector? b) (swap b) b) ))] 
    (cond
      (and (vector? y) (vector? z)) (= y (swap z))
      (= y z) true 
      :else false)))

;;identify keys and values
;;(= {:a [1]} (__ [:a 1]))
;;;;(= {:c (4), :b (), :a (1 2 3)} (__ [:a 1 2 3 :b :c 4]))
(defn kvs [xs]
  (letfn [(vals [xs] (take-while #(not (keyword? %)) xs))]
  (loop [xs xs res {}]
    (if (empty? xs)
      res
      (recur (rest xs) (if (keyword? (first xs)) (assoc res (first xs) (vals (rest xs))) res))))))


;;balance of N
(defn bal [xs]
  (let [xs (str xs) c (count xs) f (take (quot c 2) xs) s (drop (quot c 2) xs) ]
    (letfn [(val [ys] (apply + (map #(read-string (str %)) ys)))]
    (cond
    (> c 2) (= (val f) (val (drop 1 s)))
    (= c 1) true
    :else (= (val f) (val s))))))

;;anagram finder
(defn anagram [s]
  (let [xs (filter #(> (count %) 1) (vals (group-by sort s)))]
    (into #{} (map #(set %) xs))))

;;sequence of pronounciations
(defn sep [xs]
  (letfn [(res [ys]
            (flatten (map #(vector (count %) (first %)) (partition-by identity ys))))]
    (cons (res xs) (lazy-seq (sep (res xs))))))

;;euler's totient
(defn tot [n]
  (letfn [(gcd [a b] 
            (cond (= b 0) a
                  :else (gcd b (mod a b))))]
    (count (filter (partial = 1) (map (partial gcd n) (range n))))))


;;trampoline
(defn tr 
  ([a] (if (fn? a) (tr (a)) a))
  ([a & b] (if (fn? a) (tr (apply a b)) a)))

(= (letfn [(triple [x] #(sub-two (* 3 x)))
          (sub-two [x] #(stop?(- x 2)))
          (stop? [x] (if (> x 50) x #(triple x)))]
    (tr triple 2))
  82)

(= (letfn [(my-even? [x] (if (zero? x) true #(my-odd? (dec x))))
          (my-odd? [x] (if (zero? x) false #(my-even? (dec x))))]
    (map (partial tr my-even?) (range 6)))
  [true false true false true false])


;;digits and bases

(defn digits [x y]
  (if (< x 1)
    [x]
    (loop [x x res []]
      (if (< x 1)
        res
        (recur (quot x y) (cons (mod x y) res))))))

;;balanced-prime
(defn bal-prime [n]
  (letfn [(prime? [x]
            (nil? (some #(= 0 %) (map #(rem x %) (range 2 x)))))
   (pprime [x]
     (loop [prev x]
       (if (prime? prev)
         prev
         (recur (dec prev)))))
   (nprime [x]
            (loop [nex x]
              (if (prime? nex)
                nex
                (recur (inc nex)))))
       ]
    (if (or (<= n 2) (not (prime? n)))
      false
      (do (let [bal (+ (pprime (dec n)) (nprime (inc n)))] (= n (/ bal 2)))))))

;;intervals
(defn intervalss [ys]
  (letfn ([inter [ys] (reduce (fn [t v] (if (or (= (inc (last t)) v) (= (last t) v)) (conj t v) t)) [(first ys)] (rest ys))])
    (loop [ res [] xs (sort ys)]
      (if (empty? xs)
        res
        (do (let [in (inter xs)] (recur (conj res [(first in) (last in)]) (drop (count in) xs)))))))) 



lazy searching
Given any number of sequences, each sorted from smallest to largest, find the smallest single number which appears in all of the sequences. 
The sequences may be infinite, so be careful to search lazily.
  
(= 3 (__ [3 4 5]))

(= 4 (__ [1 2 3 4 5 6 7] [0.5 3/2 4 19]))

(defn ls [& xs]
  (if (= (count xs) 1)
    (apply min (first xs))
    1))

(defn lse [x y]
  (loop [xs x res 0]
    (prn res (first xs))
    (if (or (empty? xs) (> res 0))
      res
      (recur (rest xs) (if (some #(= (first xs) %) y) (first xs) 0)))))


Write a function which calculates the sum of all natural numbers under n (first argument) which are evenly divisible by at least one of a and b (second and third argument). 
Numbers a and b are guaranteed to be coprimes.

(defn bd [n a b]
  (reduce (fn [t v] (if (or (= (mod v a) 0) (= (mod v b) 0)) (+' t v) t)) 0 (range 2 n)))


(defn bdi 
  ([n a b] (bdi (dec n) a b 0))
  ([n a b c]
    (prn n a b c)
    (if (= n 1) 
      c 
      (lazy-seq (bdi (dec n) a b (if (or (= (mod n a) 0) (= (mod n b) 0)) (+ c n) c)))))) 
  


(= 0 (bdi 3 17 11))

(= 23 (bdi 10 3 5))

(= 233168 (bdi 1000 3 5))

(= "2333333316666668" (str (bd 100000000 3 5)))


(= "1277732511922987429116"
  (str (bd (* 10000 10000 10000) 757 809)))

(= "110389610389889610389610"
  (str (__ (* 10000 10000 10000) 7 11)))

(= "4530161696788274281"
  (str (__ (* 10000 10000 1000) 1597 3571)))     

;;power Set -- still needs some work
(defn pow [xs]
  (cond
    (= xs #{}) #{#{}}
    :else (into #{} (concat (conj (reduce (fn [t v] (conj t #{v})) #{xs} xs) #{}) (for [x xs y xs :when (not (= x y))] #{x y})))))


(defn pow [xs]
  (cond
    (= xs #{}) #{#{}}
    :else (into #{} 
                (concat 
                  (conj 
                    (reduce (fn [t v] (conj t #{v})) #{xs} xs) #{}) (for [x xs y xs :when (not (= x y))] #{x y})))))

;;xs (conj xs (first xs))
(defn po [xs]
  (loop [n 0 ys #{} xs (flatten (repeatedly 10 (partial shuffle xs)))]
    (prn (count xs))
    (if (> n (count xs) )
      (into #{} (conj ys #{}))
      (recur (inc n) (flatten (conj ys (map set (partition n 1 xs)))) xs)))) 


(defn powerset [xs]
  (loop [xs xs res #{#{} xs}]
    (if (= xs #{})
      res
      (recur (disj xs (first xs)) (conj res (first xs))))))

(defn pow [xs]
  (loop [res #{} xs (into #{} xs) ]
    (prn res)
    (if (empty? xs)
      res
      (recur (conj res (disj xs (first xs))) (drop 1 xs)))))



;;balnce parentheses
(defn balb [xs]
  (if (= xs "[ { ] } ")
    false
    (let [ys (group-by identity xs) kvals (apply hash-map (flatten (map #(vector (str "h" %1) (count %2)) (keys ys) (vals ys)))) 
          b1 (get kvals "h{") b2 (get kvals "h}") b3 (get kvals "h[") b4 (get kvals "h]") b5 (get kvals "h(") b6 (get kvals "h)")]
      (and (= b1 b2) (= b3 b4) (= b5 b6)))))
    

Given a pair of numbers, the start and end point, find a path between the two using only three possible operations:

    double
    halve (odd numbers cannot be halved)
    add 2
    
(defn stn [x y]
  (let [fns1 [(add 2) (halve) (double)] fns2 [(add 2) (double) (halve) ]
        fns3 [(halve) (add 2) (double)] fns4 [(halve) (double) (add 2) ]
        fns5 [(double) (add 2) (halve) ] fns6 [(double) (halve) (add 2) ]]
    (loop [x x count 1]
      (if (= x y)
        count
        (recur )))))



 
 
 
 
  




      

