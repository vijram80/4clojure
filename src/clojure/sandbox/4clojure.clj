(ns sandbox.4clojure)

;;4clojure #26
(defn fib [n]
  (loop [n (- n 2) [n0 n1 :as result] [1 1]]
    (if (<= n 0)
      (reverse result)
      (recur (dec n) (cons (+ n0 n1) result)))))
        
;;4clojure #26
(defn fib2 [n]
  (->> [1 1] 
    (iterate #(cons (+ (first %) (second %)) %))
    (take (dec n))
    last
    reverse))
        
;;4clojure #43
(defn rev-inter [xs n]
  (->> (interleave (flatten (repeat (range n))) xs)
    (partition 2)
    (group-by first)
    vals
    (map (partial map second))))

;;4clojure #44
(defn rot
  "rotate collection xs by n positions"
  [n xs]
  (let [n (->> xs count (mod n) inc)
        rot1 (fn [xs] (concat (rest xs) [(first xs)]))]
    (->> (iterate rot1 xs) (take n) last)))
