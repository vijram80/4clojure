(ns sandbox.mainclj
  (:import [sandbox.java Person Status])
  (:gen-class))

(defn -main [& args]
  (println "Hello world from Clojure.")
  (let [p (doto (Person. "John" "Smith")
            (.setStatus Status/OFFLINE))]
    (println (str p))
    (.setStatus p Status/ONLINE)
    (println (str p)))) 
    
