(ns sandbox.bug.fix)

(defn Greeter
	(greet [this other-name]))

(defn Person [^String name]
  Greeter
  (greet [this other-name] 
    (str "Hi " other-name ", my name is " this)))