(ns sandbox.file
  (:require [clojure.java.io :refer [reader]]))

(defn process-file
  "Process a text file by calling function f on each line of the file
   Some sample usage:
     (process-file count \"/log.txt\")
     (process-file last \"/log.txt\")
     (process-file (fn [lines] (doseq [l lines] (println l))) \"/log.txt\")
     (process-file #(doall (partition 2 %)) \"/log.txt\")"
  [f fname]
  (with-open [rdr (reader fname)]
    (f (line-seq rdr))))

(defn find-pattern
  "Find instances of a a regular expression pattern in a file."
  [fname pattern]
  (let [pred (partial re-find pattern)]
    (process-file 
      (fn [c] (doall (filter pred c)))
      fname)))

(defn find-pattern2
  "Find instances of a a regular expression pattern in a file."
  [fname pattern]
  (let [pred (partial re-find pattern)
        reducer (fn [t v] (if (pred v) (conj t v) t))]
    (process-file 
      (fn [c] (reduce reducer [] c))
      fname)))

(defn write-file 
  "Sample text file writer"
  []
  (let [msg "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"]
    (with-open [w (clojure.java.io/writer "/data/bar.txt")]
      (dotimes [n 50000000] (.write w (str msg "\n"))))))